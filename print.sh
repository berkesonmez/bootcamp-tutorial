#!/bin/bash
IFS=$'\n'
echo -e "\n\n--------------------- ENVIRONMENT VARIABLES --------------------\n\n"
START="false"
for LINE in $(cat configs.yml); do
  KEY=$(echo $LINE | cut -d: -f1 | xargs)
  if [ "$START" == "true" ]; then
    echo $(echo $KEY | sed 's/\./_/g' | sed 's/-/_/g' | tr '[:lower:]' '[:upper:]')=${LINE#*: } | xargs
  fi
  if [ "$KEY" == "data" ]; then
    START="true"
  fi
done
START="false"
for LINE in $(cat secrets.yml); do
  KEY=$(echo $LINE | cut -d: -f1 | xargs)
  if [ "$START" == "true" ]; then
    echo $(echo $KEY | sed 's/\./_/g' | sed 's/-/_/g' | tr '[:lower:]' '[:upper:]')=$(echo ${LINE#*: } | base64 -d | xargs)
  fi
  if [ "$KEY" == "data" ]; then
    START="true"
  fi
done
echo -e "\n\n--------------------- ENVIRONMENT VARIABLES --------------------\n\n"
